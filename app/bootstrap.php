<?php
/**
 * Бутстрап приложения
 */

// Все кодировки - UTF-8
mb_internal_encoding('UTF-8');

require_once dirname(__FILE__) . '/../var/bootstrap.php.cache';

/** @var bool флаг нужна ли отлкадка */
$debug = false;

/** @var bool находимся ли в cli */
$cli = false;

// Проверка типа окружения
if (php_sapi_name() == 'cli') { // запуск в cli
    set_time_limit(0);
    $cli = true;

    $ArgvInput = new \Symfony\Component\Console\Input\ArgvInput();
    $env = mb_strtolower($ArgvInput->getParameterOption(array('--env', '-e'), getenv('ENVIRONMENT') ?: ''));

    if (empty($env)) {
        print 'Undefined ENVIRONMENT' . PHP_EOL;
        die();
    }
} else { // запуск в web
    if (!isset($_SERVER['ENVIRONMENT'])) {
        die();
    }
    $env = mb_strtolower($_SERVER['ENVIRONMENT']);
}

define('ENVIRONMENT_LEVEL', mb_strtolower($_SERVER['ENVIRONMENT']));
$debug = ($env === 'dev');

// наше ядро
require_once 'AppKernel.php';

if ($debug) {
    \Symfony\Component\Debug\Debug::enable();
}