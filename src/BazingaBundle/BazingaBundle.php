<?php

namespace BazingaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Тестовый бандл, для изучения возможностей фреймворка
 * @author t.khavrenkova
 */
class BazingaBundle extends Bundle {

}
