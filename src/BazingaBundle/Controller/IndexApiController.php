<?php
namespace BazingaBundle\Controller;

use EngineBundle\Controller\ApiController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @author t.khavrenkova
 */
class IndexApiController extends ApiController {

    /**
     * @Route(
     *      "/bazinga/api",
     *      defaults={"_format"="json"},
     *      name="api_index"
     * )
     * @Template()
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $data = array('bla' => 123);

        return $this->setJsonResponse($data);
    }

    /**
     * Возврат json-данных на основе передаваемого в адресной строке параметра
     *
     * @Route(
     *      "/bazinga/api/info/{id}",
     *      defaults={"_format"="json"},
     *      requirements={"id"="\d+"},
     *      name="api_info"
     * )
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function infoAction($id) {
        $data = array('id' => $id);
        return $this->setJsonResponse($data);
    }

    /**
     * @Route(
     *      "/bazinga/api/not-found",
     *      name = "api_not_found"
     * )
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function notFoundAction() {
        throw $this->createNotFoundException('Муа-ха-ха');
    }
}