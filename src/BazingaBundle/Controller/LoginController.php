<?php
namespace BazingaBundle\Controller;

use EngineBundle\Controller\EngineController;
use EngineBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * @author t.khavrenkova
 */
class LoginController extends EngineController {
    /**
     * @Route(
     *      "/bazinga/login",
     *      name="bazinga_login_form"
     * )
     *
     * @param \Symfony\Component\HttpFoundation\Request $Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $Request) {
        $Session = $Request->getSession();

        if ($Request->get('login')) {
            // ищем такого юзера в БД
            $email = $Request->get('email');
            $password = $Request->get('password');

            $Repository = $this->getDoctrine()->getRepository('EngineBundle:User');
            $encodePassword = User::encodePassword($password);

            /** @var User|null $User */
            $User = $Repository->findOneBy(array(
                'email' => $email,
                'password' => $encodePassword
            ));

            if ($User) {
                $Session->set('admin_id', $User->getId());
                $Session->set('admin_hash', $User->getHash());
            } else {
                echo "Неверный логин/пароль";
            }
        }

        if ($Session->has(User::SESSION_ADMIN_VAR_ID) && $Session->has(User::SESSION_ADMIN_VAR_HASH)) {
            return $this->redirect($this->generateUrl('bazinga_admin_index'));
        }

        return $this->render('BazingaBundle:Login:index.html.smarty', array(
            'email' => isset($email) ? $email : ''
        ));
    }

    /**
     * @Route(
     *      "/bazinga/logout",
     *      name="bazinga_logout"
     * )
     */
    public function logoutAction(Request $Request) {
        $Session = $Request->getSession();
        if ($Session->has(User::SESSION_ADMIN_VAR_ID) &&
            $Session->has(User::SESSION_ADMIN_VAR_HASH))
        {
            $Session->remove(User::SESSION_ADMIN_VAR_ID);
            $Session->remove(User::SESSION_ADMIN_VAR_HASH);
        }
        return $this->redirect($this->generateUrl('_index'));
    }
}