<?php
namespace BazingaBundle\Controller;

use EngineBundle\Controller\ApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для возврата json-данных в админ. разделе
 * Все наследуемые от него контроллеры будут проверять юзера на роль админа
 *
 * @author t.khavrenkova
 */
class AdminApiController extends ApiController {

}