<?php
namespace BazingaBundle\Controller;

use BazingaBundle\Document\MongoExampleEntity;
use Doctrine\Common\Persistence\ObjectManager;
use EngineBundle\Controller\EngineController;
use EngineBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * @author t.khavrenkova
 */
class IndexController extends EngineController {

    /**
     * Меню - список демонстрируемых возможностей бандла
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $formUrl     = $this->generateUrl('_index_form');
        $usersUrl    = $this->generateUrl('users_list');
        $apiUrl      = $this->generateUrl('api_index');
        $apiInfoUrl  = $this->generateUrl('api_info', array('id' => rand(1, 100)));
        $notFoundUrl = $this->generateUrl('api_not_found');
        $kittensUrl  = $this->generateUrl('kittens');
        $redisUrl    = $this->generateUrl('redis_example');
        $adminUrl    = $this->generateUrl('bazinga_admin_index');
        $loginUrl    = $this->generateUrl(AdminController::ADMIN_LOGIN_ROUTE);
        $logoutUrl   = $this->generateUrl(AdminController::ADMIN_LOGOUT_ROUTE);

        $data = array(
            'urls' => array(
                array('url' => $formUrl,     'name' => 'Пример работы с формой, валидация данных, создание юзера-админа'),
                array('url' => $usersUrl,    'name' => 'Пример выборки из БД'),
                array('url' => $apiUrl,      'name' => 'Пример возвращения данных в формате json'),
                array('url' => $apiInfoUrl,  'name' => 'json-данные на основе параметра в адресной странице'),
                array('url' => $notFoundUrl, 'name' => 'Пример насильного возвращения 404 страницы при переходе по существующему роуту'),
                array('url' => $redisUrl,    'name' => 'Пример работы с редисом'),
                array('url' => $adminUrl,    'name' => 'Адинистративный раздел. Неавторизованный пользователь будет перенаправлен на форму авторизации.'),
                array('url' => $loginUrl,    'name' => 'Форма авторизации для административного раздела'),
                array('url' => $logoutUrl,   'name' => 'Выход из административного раздела'),
                array('url' => $kittensUrl,  'name' => 'Ну и напоследок котятки :)'),
            ),
            'curDate' => date('Y-m-d H:i:s'),
        );
        return $this->render('BazingaBundle:Index:index.html.smarty', $data);
    }

    /**
     * Страница с формой создания пользователя
     *
     * @param Request $Request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formAction(Request $Request) {
        $data = array();
        if ($Request->get('submit')){
            $data = $Request->request->all();

            $UserModel = new User();
            $UserModel
                ->setEmail($Request->get('email', null))
                ->setName($Request->get('name', null))
                ->setPlainPassword($Request->get('password', null))
                ->setRoleId(User::ROLE_ADMIN)
                ->setHash()
            ;

            $Validator = $this->getValidator();
            $errors = $Validator->validate($UserModel);

            if (count($errors) > 0) {
                /** @var ConstraintViolation[] $errors */
                foreach ($errors as $error) {
                    echo $error->getMessage() . '<br>';
                }
            } else {
                $Em = $this->getDoctrine()->getManager();
                $Em->persist($UserModel);
                $Em->flush();

                echo 'User created, id = ' . $UserModel->getId();
            }
        }

        return $this->render('BazingaBundle:Index:form.html.smarty', $data);
    }

    /**
     * Список сохраненных юзеров из БД
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersListAction() {

        /** @var User[] $Users */
        $Users = $this->getDoctrine()
            ->getRepository('EngineBundle:User')
            ->findAll();

        $usersData = array();
        foreach ($Users as $User) {
            $usersData[] = array(
                'id' => $User->getId(),
                'name' => $User->getName(),
                'email' => $User->getEmail(),
            );
        }

        return $this->render('BazingaBundle:Index:users.html.smarty', array(
            'users' => $usersData
        ));
    }

    /**
     * Инкремент переменной из редиса
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function redisAction() {
        $Redis = $this->getRedis();

        $Redis->incr('views:cnt');
        $views = $Redis->get('views:cnt');

        return $this->render('BazingaBundle:Index:redis.html.smarty', array(
            'viewsCount' => $views
        ));
    }

    /**
     * Просто котятки
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function kittensAction() {
        return $this->render('BazingaBundle:Index:kittens.html.smarty');
    }

    /**
     * Пример работы с монго
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mongoAction() {
        $Mongo = new MongoExampleEntity();
        $Mongo
            ->setName('mongo')
            ->setPrice(123);

        /** @var ObjectManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($Mongo);
        $dm->flush();

        return $this->render('BazingaBundle:Index:mongo.html.smarty', array('id' => $Mongo->getId()));
    }

    /**
     * Пример работы с переводами
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translationAction() {
        $translator = new Translator('ru_RU');
        $translator->addLoader('yaml', new YamlFileLoader());
        $translator->addResource('yaml', 'messages.ru.yml', 'ru_RU');
echo '<pre>'; print_r($translator);
        echo $translator->trans('bla');
        echo $translator->trans('Symfony2 is great');

        return $this->render('BazingaBundle:Index:translation.html.smarty', array());
    }
}