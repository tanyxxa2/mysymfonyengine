<?php
namespace BazingaBundle\Controller;

use EngineBundle\Controller\EngineController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Закрытый раздел только для админов
 * @author t.khavrenkova
 */
class AdminController extends EngineController {

    /** Роут для формы авторизации в административный раздел */
    const ADMIN_LOGIN_ROUTE = 'bazinga_login_form';

    /** Роут для выхода из административного раздела */
    const ADMIN_LOGOUT_ROUTE = 'bazinga_logout';

    /** Название переменной в сессии, в которой хранится идентификатор авторизованного админа */
    const ADMIN_SESSION_VAR_NAME = 'admin_id';

    /**
     * @Route(
     *      "/bazinga/admin",
     *      name="bazinga_admin_index"
     * )
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        $logoutUrl = $this->generateUrl(self::ADMIN_LOGOUT_ROUTE);

        return $this->render('BazingaBundle:Admin:index.html.smarty', array(
            'logout_url' => $logoutUrl
        ));
    }

}