<?php
namespace EngineBundle\EventListener;

use EngineBundle\Logger\Logger;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Событие, вызываемое перед выполенением контроллера
 * @author t.khavrenkova
 */
class ExceptionListener {

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $Exception = $event->getException();
        Logger::getInstance()->error($Exception);
    }
}