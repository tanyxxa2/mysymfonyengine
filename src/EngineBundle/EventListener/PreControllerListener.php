<?php
namespace EngineBundle\EventListener;

use BazingaBundle\Controller\AdminApiController;
use EngineBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use BazingaBundle\Controller\AdminController;

/**
 * Событие, вызываемое перед выполенением контроллера
 * @author t.khavrenkova
 */
class PreControllerListener {

    /**
     * @param FilterControllerEvent $event
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $Controller = $event->getController();
        $ControllerObj = isset($Controller[0]) ? $Controller[0] : null; // срань какая-то, но работает (

        /** @var Controller $ControllerObj */
        if ($ControllerObj instanceof AdminController || $ControllerObj instanceof AdminApiController) {
            /** @var Request $Request */
            $Request = $ControllerObj->get('request_stack')->getCurrentRequest();
            $Session = $Request->getSession();
            $redirectUrl = $ControllerObj->generateUrl(AdminController::ADMIN_LOGIN_ROUTE);
            if ($Session->has(User::SESSION_ADMIN_VAR_ID) && $Session->has(User::SESSION_ADMIN_VAR_HASH)) {

                //дополнительная проверка, что hash именно этого пользователя
                $User = $ControllerObj->getDoctrine()
                    ->getRepository('EngineBundle:User')
                    ->findOneBy(array(
                        'id'      => $Session->get(User::SESSION_ADMIN_VAR_ID),
                        'hash'    => $Session->get(User::SESSION_ADMIN_VAR_HASH),
                        'role_id' => User::ROLE_ADMIN
                    ));
                if (!$User) {
                    $event->setController(function() use ($redirectUrl) {
                        return new RedirectResponse($redirectUrl);
                    });
                }

            } else {
                $event->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }
        }
    }
}