<?php
namespace EngineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Redis;

/**
 * Базовый контроллер, от котрого будем наследовать все остальные контроллеры.
 * Дописываем сюда все вспомогательные методы
 *
 * @author t.khavrenkova
 */
class EngineController extends Controller {

    /**
     * Объект валидатора
     *
     * @return ValidatorInterface
     */
    public function getValidator() {
        return $this->get('validator');
    }

    /**
     * Объект работы с редисом
     * @return Redis
     */
    public function getRedis() {
        return $this->container->get('snc_redis.default');
    }
}