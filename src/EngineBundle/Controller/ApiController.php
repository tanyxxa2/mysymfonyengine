<?php
namespace EngineBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для возврата json-данных
 * @author t.khavrenkova
 */
class ApiController extends EngineController {

    /**
     * Ковертация массива в json-объект и
     * передача на страницу в формате json вместо html
     *
     * @param array $data
     * @return Response
     */
    protected function setJsonResponse($data) {
        $response = new Response(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}