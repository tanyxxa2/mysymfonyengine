<?php
namespace EngineBundle\SmartyExtension;

use NoiseLabs\Bundle\SmartyBundle\Extension\AbstractExtension;
use NoiseLabs\Bundle\SmartyBundle\Extension\Plugin\ModifierPlugin;

/**
 * @author t.khavrenkova
 */
class DateExtension extends AbstractExtension {

    /**
     * @return array
     */
    public function getPlugins()
    {
        return array(
            new ModifierPlugin('dateFormat', $this, 'getDateModifier')
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName() {
        return 'date';
    }

    /**
     * @param string $date
     * @return bool|string
     */
    public function getDateModifier($date) {
        return date('d.m.Y H:i', strtotime($date));
    }
}