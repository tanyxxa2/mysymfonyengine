<?php
namespace EngineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EngineBundle\Entity\UserRepository")
 * @UniqueEntity(
 *      fields="email",
 *      message="Такой email уже зарегистрирован"
 * )
 */
class User
{
    const SALT = 'bla-bla-bla-salt';

    /** Роль "пользователь" */
    const ROLE_USER  = 1;

    /** Роль "админ" */
    const ROLE_ADMIN = 2;

    /**
     * Названия переменных сессии,
     * где будут хранится данные авторизованных пользователей
     * Отдельные переменные для админа/юзера/.. для возможности авторизации под несколькими пользователями
     */
    const SESSION_USER_VAR_ID    = 'user_id',
          SESSION_USER_VAR_HASH  = 'user_hash',
          SESSION_ADMIN_VAR_ID   = 'admin_id',
          SESSION_ADMIN_VAR_HASH = 'admin_hash';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @var string
     */
    protected $plainPassword;

    /**
     * Уникальный хеш пользователя
     * По нему будем проверять корректность залогиненного пользователя
     *
     * @ORM\Column(name="hash", type="string", length=255)
     * @var string
     */
    protected $hash;

    /**
     * Роль пользователя, по умолчанию простой юзер
     *
     * @ORM\Column(name="role_id", type="integer")
     * @var int
     */
    protected $role_id = self::ROLE_USER;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @Assert\NotBlank(
     *   message = "Имя не может быть пустым"
     * )
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @Assert\Email(
     *     message = "'E-mail {{ value }} введен неверно'",
     *     checkMX = true
     * )
     * @Assert\NotBlank(
     *   message = "'Email не может быть пустым'"
     * )
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     * @Assert\NotBlank(
     *   message = "'Пароль не может быть пустым'"
     * )
     * @return string 
     */
    public function getPassword()
    {
        if (is_null($this->password)) {
            $this->password = self::encodePassword($this->getPlainPassword());
        }
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setPlainPassword($value) {
        $this->plainPassword = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash() {
        if (is_null($this->hash)) {
            $this->hash = $this->generateUserHash();
        }
        return $this->hash;
    }

    /**
     * @return $this
     */
    public function setHash() {
        $this->hash = $this->generateUserHash();
        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId() {
        return $this->role_id;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setRoleId($value) {
        $this->role_id = $value;
        return $this;
    }

    /**
     * Генерация уникального хеша пользователя
     * @return string
     */
    protected function generateUserHash() {
        $hash = $this->getPassword() . time();
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);

        return $encoder->encodePassword($hash, self::SALT);
    }

    /**
     * Шифрование пароля
     *
     * @param string $password
     * @return string
     */
    public static function encodePassword($password) {
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        return $encodePassword = $encoder->encodePassword($password, User::SALT);
    }
}
