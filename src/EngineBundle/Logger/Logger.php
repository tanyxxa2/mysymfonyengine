<?php

namespace EngineBundle\Logger;
use Symfony\Component\DependencyInjection\Container;

/**
 * Класс для работы с логами проекта
 *
 * @author t.khavrenkova
 */
class Logger {
    /**
     * Типы логов
     */
    const TYPE_ERROR = 'ERROR';
    const TYPE_INFO  = 'INFO';
    const TYPE_WARN  = 'WARNING';

    /**
     * @var string Название файла, куда будут записываться логи
     */
    private $logName;

    /**
     * @var Logger Инстанс логгера
     */
    private static $Instance;

    /**
     * @param string $logName
     * @return Logger
     */
    public static function getInstance($logName = 'Insatiable') {
        if (is_null(self::$Instance)) {
            self::$Instance = new Logger($logName);
        }
        return self::$Instance;
    }

    private function __construct($logName) {
        $this->logName = $logName;
    }

    /**
     * @return string
     */
    public function getFileName() {
        return $this->logName;
    }

    /**
     * Запись ошибки в лог
     * @param string $error
     */
    public function error($error) {
        $this->log(self::TYPE_ERROR, $error);
    }

    /**
     * Запись вспомогательной информации в лог
     * @param string $info
     */
    public function info($info) {
        $this->log(self::TYPE_INFO, $info);
    }

    /**
     * Запись нефатальных ошибок в лог
     * @param string $warn
     */
    public function warn($warn) {
        $this->log(self::TYPE_WARN, $warn);
    }

    /**
     * Логирование
     *
     * @param string $type Тип лога
     * @param string $text Логируемое сообщение
     * @todo Как-то покрасивее надо путь получать
     */
    private function log($type, $text) {
        $logsDir = dirname(__FILE__) . '/../../../var/logs/';
        $filePath = $logsDir . $this->getFileName() . '.log';

        $date = date('d-m-Y H:i:s');
        $msg = $date . ' | ' . $type . ' | ' . $text . "\n";

        // дозаписываем файл
        $file = fopen($filePath, 'a+');
        fwrite($file, $msg);
        fclose($file);
    }

}