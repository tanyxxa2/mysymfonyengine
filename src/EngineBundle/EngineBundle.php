<?php
namespace EngineBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл для движка. Сюда будет складывать все сущности, относящиейся к любому абстрактному бандлу
 * @author t.khavrenkova
 */
class EngineBundle extends Bundle {

}
