### What is this repository for? ###

Базовый набор инструментов для работы с Symfony Framework + примеры использования.

### How do I get set up? ###

Step 1. git clone https://tanyxxa@bitbucket.org/tanyxxa/symfonyengine.git

Step 2. composer update

Step 3. Настроить nginx или другой используемый веб-сервер.

Примерный конфиг для nginx:

```
#!python
server {
    server_name symfony.local;
    root /path/to/symfonyengine/web;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/(index)\.php(/|$) {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param ENVIRONMENT dev;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }

    error_log /path/to/logs/error.log;
    access_log /path/to/logs/access.log;
}

```

Для Apache:

```
#!python
<VirtualHost *:80>
    ServerName sym.loc
    ServerAlias www.sym.loc
    SetEnv ENVIRONMENT dev

    DocumentRoot "D:/W/PROJECTS/symengine/web/"
    <Directory "D:/W/PROJECTS/symengine/web/">
        # enable the .htaccess rewrites
        AllowOverride All
        Order allow,deny
        Allow from All
    </Directory>

    ErrorLog "D:/W/PROJECTS/symengine/var/logs/sym.localhost-error.log"
    CustomLog "D:/W/PROJECTS/symengine/var/logs/sym.localhost-access.log" common
</VirtualHost>


```