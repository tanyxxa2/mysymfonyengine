<?php

use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../app/bootstrap.php';

if ($cli) {
    $AppKernel = new AppKernel(ENVIRONMENT_LEVEL, $debug);
    $Application = new \Symfony\Bundle\FrameworkBundle\Console\Application($AppKernel);
    $Application->run($ArgvInput);
} else {
    $AppKernel = new AppKernel(ENVIRONMENT_LEVEL, $debug);
    $AppKernel->loadClassCache();
    $Request = Request::createFromGlobals();
    $Response = $AppKernel->handle($Request);
    $Response->send();
    $AppKernel->terminate($Request, $Response);
}